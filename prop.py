#Module which includes functions to calculate magnetisation and energy for a state

#Import relevant modules
import ising as isn
import numpy as np

#Function to calculate magnetisation of lattice
def mag(A):
	mag1 = np.sum(A) #Add up all spins in matrix
	return mag1

#Function to calculate energy of lattice
def energy(A):
	n = A.shape[0] 	 #Store dimension of A locally
	egy = 0
	#Loop over each entry of lattice
	for j in range(n):
		for k in range(n):
			egy += -isn.nbrs(A, j, k)*A[j, k] #Add contribution of each site to energy
	
	egy = egy/4 #Divide by 4 to eliminate overcounting of spin pairs (as included in Hamiltonian)
	return egy 


