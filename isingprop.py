#Script which investigates properties of system at low tempertures, searching for phase transition

#Import relevant modules
import ising as isn
import matplotlib.pyplot as plt
import numpy as np
import prop
import sys

#Set physical parameters of the experiment
J = float(sys.argv[1])      #Interaction energy
n = int(float(sys.argv[2])) #Scale of system, size of lattice is nxn
h = float(sys.argv[3])      #External magnetic field
N = 100                     #Number of temperatures tested
	
#Setting up array of equally spaced temperatures, and arrays to store properties
T = np.linspace(1.5, 3.5, N)
mags = np.zeros(N)
suscs = np.zeros(N)
egys = np.zeros(N)
heats = np.zeros(N)

time1 = int(float(sys.argv[4])) #Time for equilibrium
time2 = int(float(sys.argv[5])) #Time calculating properties after equilibrium

#Loop over each value of temperature and calculate properties
for p in range(N):
	A = isn.init(n) #Initialise matrix
	#Get system close to equilibrium by updating time1 times
	for j in range(time1):
		isn.update(A, T[p], J, h)

	#Set initial variables to store mag, mag^2, energy, energy^2
	m = 0
	m2 = 0
	e = 0
	e2 = 0
	
	#Update system time2 times and then calculate properties
	for j in range(time2):
		isn.update(A, T[p], J, h)
		#Calculate magnetisation and energy at each time
		mag1 = prop.mag(A)
		egy1 = prop.energy(A)
		
		#Increment all the values mag, mag^2, energy, energy^2 at each stage
		e += egy1
		e2 += egy1**2
		m += mag1
		m2 += mag1**2

	#Calculating average energy and average magnetisation at each temperature
	egys[p] = (e/(time2*n**2))
	mags[p] = np.abs(m/(time2*n**2))
		
	#Similarly calculating specific heat and susceptibility according to expection formula
	heats[p] = (e2/time2 - e**2/(time2**2))/(n**2*T[p]*T[p])
	suscs[p] = (m2/time2 - m**2/(time2**2))/(n**2*T[p]*T[p])
	print T[p] #Printing T, sanity check

#Open datafiles to write in, glue respective data arrays together, transpose, and save to data file	
datafile_id = open("mag.dat", 'w+')
data = np.array([T, mags])
data = data.T
np.savetxt(datafile_id, data, fmt = ['%f', '%f'])
datafile_id.close()

datafile_id = open("suscs.dat", 'w+')
data = np.array([T, suscs])
data = data.T
np.savetxt(datafile_id, data, fmt = ['%f', '%f'])
datafile_id.close()

datafile_id = open("egys.dat", 'w+')
data = np.array([T, egys])
data = data.T
np.savetxt(datafile_id, data, fmt = ['%f', '%f'])
datafile_id.close()

datafile_id = open("heats.dat", 'w+')
data = np.array([T, heats])
data = data.T
np.savetxt(datafile_id, data, fmt = ['%f', '%f'])
datafile_id.close()

