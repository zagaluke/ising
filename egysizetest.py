#Script which investigates the effects of lattice dimension on energy at a fixed temperature

#Import relevant modules
import ising as isn
import numpy as np
import prop
import sys

#Set physical parameters of the experiment
J = float(sys.argv[1])          #Interaction energy
h = float(sys.argv[2])          #External magnetic field
n = [5, 10, 15, 20, 25, 30]     #Scales of system tested, size of lattice is nxn

#Make array of n^2 for plotting 
n2 = np.zeros(len(n))
for j in range(len(n)):
	n2[j] = n[j]**2
	
T = float(sys.argv[3])          #Temperature of system
egys = np.zeros(len(n))         #Array to store energy for different lattice sizes

time1 = int(float(sys.argv[4])) #Time for equilibrium
time2 = int(float(sys.argv[5])) #Time calculating properties after equilibrium

#Loop over array of dimensions
for z in range(len(n)):

	A = isn.init(n[z])          #Initialise matrix
	#Get system close to equilibrium by updating time1 times
	for j in range(time1):
		isn.update(A, T, J, h)
	
	
	e = 0                       #Set initial variable to store energy
		
	#Update system time2 times and then calculate properties
	for j in range(time2):
		isn.update(A, T, J, h)
		egy1 = prop.energy(A) 	#Calculate energy at each time	
		e += egy1               #Increment the value of e at each stage
	
	egys[z] = (e/(time2))       #Set total energy for each lattice size

	print n[z]                  #Print n, sanity check

#Open datafile to write in, glue arrays together, transpose, and save to data file	
datafile_id = open("egysizetest.dat", 'w+')
data = np.array([n2, egys])
data = data.T
np.savetxt(datafile_id, data, fmt = ['%f', '%f'])
datafile_id.close()





