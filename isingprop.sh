#!/bin/bash
#Takes input from command line and runs isingprop.py

#Read relevant parameters
echo -e "Please enter a value for the interaction coefficient J: "
read J

echo -e "Please enter a value for the lattice dimension n: "
read n

echo -e "Please enter a value for the external magnetic field h: "
read h

echo -e "Please enter a value for equilibrium time (Recommended around 500): "
read t1

echo -e "Please enter a value for computing time (Recommended around 3000): "
read t2

#Run script with inputted parameters
python isingprop.py $J $n $h $t1 $t2

echo -e "Done!\n"

#Plot each data file in sequence
xmgrace mag.dat
xmgrace suscs.dat
xmgrace egys.dat
xmgrace heats.dat

#Giving user option to remove files that were saved by script
echo -e "Do you want to remove the data files? reply y/n: "
read -n 1 a

if (("$a"=="y"))
then
	rm mag.dat suscs.dat egys.dat heats.dat #Remove all files
	echo " "
	echo "FILES EXTERMINATED"
elif (("$a"=="n"))
then
	echo " "
	echo "Your files are still there, as requested!"
else
	echo " "
	echo "You didn't follow instructions, but files still there!"
fi
