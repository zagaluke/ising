#Plotting module

#Import relevant module
import numpy as np

#Function which plots a square matrix of 1's and 0's as a pbm file
def myplot(A, file):
	n = A.shape[0] 	                      #Store dimension of matrix A locally
	f = open(file, 'w')                   #Open a file f in writing mode
	f.write("P1 \n")                      #Write pbm magic number P1 so it is plotted as black/white for 1/0 entries
	f.write(str(n) + " " + str(n) + "\n") #Write dimensions of A into file
	B = A.astype(bool)                    #Convert elements of A to bools	
	np.savetxt(f, B, fmt = "%i")          #Save matrix to file

