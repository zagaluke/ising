#Code which takes pbm files as command line and generates snapshot of ising model after a fixed time

#Import relevant modules
import ising as isn
import draw
import sys

#Set physical parameters of the experiment
J = float(sys.argv[1])          #Interaction energy
n = int(float(sys.argv[2]))     #Scale of system, size of lattice is nxn
T = float(sys.argv[3])          #Temperature at which to plot
h = float(sys.argv[4])          #External magnetic field
time = int(float(sys.argv[5]))  #Update time
file1 = sys.argv[6]             #Set file to save to
A = isn.init(n)                 #Initialise A

#Update system for specified time
for p in range(time):
	isn.update(A, T, J, h)	
	
A[A == -1] = 0        #Setting -1 spins to 0 for plotting
draw.myplot(A, file1) #Plot A to file1 after updates


 















