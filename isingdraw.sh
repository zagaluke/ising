#!/bin/bash
#Takes input from command line and runs isingdraw.py a number of times, saving pbm files to a number of files

#Read relevant parameters
echo -e "Please enter a value for the interaction coefficient J: "
read J

echo -e "Please enter a value for n, lattice dimension: "
read n

echo -e "Please enter a value for temperature: "
read T

echo -e "Please enter a value for the external magnetic field h: "
read h

echo -e "Please enter the interval between plots: "
read t1

#Read array of files to save snapshots to
echo -e "Enter pbm files to save snapshots to (seperated by spaces eg. f1.pbm f2.pbm f3.pbm): "
read -a TR

#Run script for each input file
for i in "${TR[@]}"
do
	echo -e "Running for $i"
	python isingdraw.py $J $n $T $h $t1 $i
done

echo -e "Done!\n"