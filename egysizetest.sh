#!/bin/bash
#Takes input from command line and runs egysizetest.py

#Read relevant parameters
echo -e "Please enter a value for the interaction coefficient J: "
read J

echo -e "Please enter a value for the external magnetic field h: "
read h

echo -e "Please enter a value for the temperature T: "
read T

echo -e "Please enter a value for equilibrium time (Recommended around 500): "
read t1

echo -e "Please enter a value for computing time (Recommended around 2500): "
read t2

#Run script with inputted parameters
python egysizetest.py $J $h $T $t1 $t2

echo -e "Done!\n"

#Plot datafile
xmgrace egysizetest.dat

#Giving user option to remove file that was saved by script
echo -e "Do you want to remove the data file? reply y/n: "
read -n 1 a

if (("$a"=="y"))
then
	rm egysizetest.dat #Remove saved file
	echo " "
	echo "FILES EXTERMINATED"
elif (("$a"=="n"))
then
	echo " "
	echo "Your files are still there, as requested!"
else
	echo " "
	echo "You didn't follow instructions, but files still there!"
fi
