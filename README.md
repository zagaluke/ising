This project implements the Ising model in Python using
the Metropolis algoritm.

There are 8 .py files, 3 of which are used as modules, and 5 of which are
used for investigation.

The 3 modules are ising.py, which implements the Metropolis algorithm and 
contains a function to update a spin state for one time step,
draw.py which plots a spin state into a pbm file, and prop.py, which includes
functions to calculate the energy and magnetisation for a spin state.

The 5 other .py files implement the above modules for different investigations
of the ising model. There are 5 .sh files which correspond to these files.
These .sh files include an interactive environment to give command line input to
the .py files, run the .py files with this input, plot any necessary data with
the useful xmgrace, and ask user whether the saved files should be removed
or deleted.

The details and aims of each of the files are explained in detail in the 
comments of the files.