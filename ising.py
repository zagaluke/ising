#Module which includes functions to initialise a random matrix and update spins according to Ising model

#Import relevant modules
import numpy as np
import random as rdm

#Function to initialise nxn matrix with entries uniform from {-1,1} 
def init(n):
	A = np.random.random_sample(n*n).reshape(n,n) 	#Initialise matrix A with pseudorandom entries in (0,1)
	B = np.rint(A) 	                                #Round entries of A to nearest integer, get a new matrix B with entries 0 or 1 
	B[B == 0] = -1 	                                #Set 0 spins to -1, this simulates picking entries uniformly from {1,-1}
	return B

#Function to add up neighbours of A[j, k] 
def nbrs(A, j, k):
	n = A.shape[0] #Store dimension of A locally
	#Implementing periodic boundary conditions using modular arithmetic since n==0 (mod n), -1==n-1 (mod n)
	h = A[(j-1) % n, k] + A[(j+1) % n, k] + A[j, (k-1) % n] + A[j, (k+1) % n]
	return h 

#Function to loop through matrix n^2 times (1 time step), randomly picking elements to update
def update(A, T, J, h):
	n = A.shape[0] #Store dimension of A locally
	for j in range(n**2):
		#For each iteration, pick 2 random integers between 0 and n-1 (these are the indices for an nxn matrix)
		x = rdm.randint(0, n-1)
		y = rdm.randint(0, n-1)
		spin = A[x, y]
		#Calculate inner field (inf) of spin, and energy change (de) due to flipping spin
		inf = J*nbrs(A, x, y) - h
		de = 2*spin*inf
		pflip = np.exp(-de/T) #Calculate flip probability due to Boltzmann statistics
		#If energy change negative, flip spin, otherwise flip according to pflip
		if de <= 0:
			spin *= -1
		#Comparing pflip to random number in [0,1]
		elif pflip > rdm.random():
			spin *= -1
		A[x, y] = spin
	return A

