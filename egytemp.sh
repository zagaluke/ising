#!/bin/bash
#Takes input from command line and runs egytemp.py

#Read array of J values
echo -e "Please enter array of values for the interaction coefficient J, seperated by spaces: "
read -a TJ

#Read other relevant parameters
echo -e "Please enter a value for the external magnetic field h: "
read h

echo -e "Please enter a value for the lattice dimension n: "
read n 

echo -e "Please enter a value for equilibrium time (Recommended around 500): "
read t1

echo -e "Please enter a value for computing time (Recommended around 1000): "
read t2

#Run script for each J value and save data to a different data file
for i in "${TJ[@]}"
do
	python egytemp.py $i $h $n $t1 $t2 egytemp$i.dat
done

echo -e "Done!\n"

#Plot all data sets on same plot for clarity, * accesses all data files
xmgrace egytemp*.dat

#Giving user option to remove files that were saved by script
echo -e "Do you want to remove the data files? reply y/n: "
read -n 1 a

if (("$a"=="y"))
then
	rm egytemp*.dat #Removes saved files 
	echo " "
	echo "FILES EXTERMINATED"
elif (("$a"=="n"))
then
	echo " "
	echo "Your files are still there, as requested!"
else
	echo " "
	echo "You didn't follow instructions, but files still there!"
fi
