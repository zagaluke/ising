#Script which investigates energy vs. temperature at large temperature scales

#Import relevant modules
import ising as isn
import numpy as np
import prop
import sys

#Set physical parameters of the experiment
J = float(sys.argv[1])          #Interaction energy
h = float(sys.argv[2])          #External magnetic field
n = int(float(sys.argv[3]))     #Scale of system tested, size of lattice is nxn
N = 100                         #Number of temperatures tested

#Make array of equally space temperatures and array to store energies
T = np.linspace(1, 20, N)
egys = np.zeros(N)

time1 = int(float(sys.argv[4])) #Time for equilibrium
time2 = int(float(sys.argv[5])) #Time calculating properties after equilibrium

#Loop over each value of temperature and calculate properties
for p in range(N):
	A = isn.init(n)             #Initialise matrix
	#Get system close to equilibrium by updating time1 times
	for j in range(time1):
		isn.update(A, T[p], J, h)
	
	e = 0 	                    #Set initial variable to store energy
		
	#Update system time2 times and then calculate properties
	for j in range(time2):
		isn.update(A, T[p], J, h)
		egy1 = prop.energy(A) 	#Calculate energy at each time
		e += egy1               #Increment the value of energy
	
	egys[p] = (e/(time2*n**2))  #Update average energy at each stage
	print T[p]                  #Print temperature, sanity check

#Open datafile to write in, glue arrays together, transpose, and save to data file	
datafile_id = open(sys.argv[6], 'w+')
data = np.array([T, egys])
data = data.T
np.savetxt(datafile_id, data, fmt = ['%f', '%f'])
datafile_id.close()

