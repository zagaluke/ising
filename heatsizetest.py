#Script which investigates the effects of lattice dimension on specific heat capacity at critical temperature

#Import relevant modules
import ising as isn
import numpy as np
import prop
import sys

#Set physical parameters of the experiment
J = float(sys.argv[1])      #Interaction energy
h = float(sys.argv[2])      #External magnetic field
n = [5, 10, 15, 20, 25, 30] #Scales of system tested, size of lattice is nxn

#Make array of log(n) for plotting
ln = np.zeros(len(n)) 
for j in range(len(n)):
	ln[j] = np.log(n[j])
	
N = 5                       #Number of temperatures tested near critical temperature

#Arrays to store heat capacities, max heat capacities for each n, and temperatures
heats = np.zeros(N)
maxheats = np.zeros(len(n))
T = np.linspace(2.1, 2.3, N)

time1 = int(float(sys.argv[3])) #Time for equilibrium
time2 = int(float(sys.argv[4])) #Time calculating properties after equilibrium

#Loop over array of dimensions
for z in range(len(n)):
	#Loop over each value of temperature near critical temperature and calculate properties
	for p in range(N):
		A = isn.init(n[z])          #Initialise matrix
		#Get system close to equilibrium by updating time1 times
		for j in range(time1):
			isn.update(A, T[p], J, h)
	
		#Set initial variables to store energy, energy^2
		e = 0
		e2 = 0
		
		#Update system time2 times and then calculate properties
		for j in range(time2):
			isn.update(A, T[p], J, h)
			egy1 = prop.energy(A) 	#Calculate energy at each time
			
			#Increment the values of energy, energy^2 at each stage
			e += egy1
			e2 += egy1**2
			
		#Updating specific heat according to expection formula
		heats[p] = (e2/time2 - e**2/(time2**2))/(n[z]**2*T[p]*T[p])

	#Storing maximum specific heat in maxheats array
	maxheats[z] = max(heats)
	print n[z]                      #Print n, sanity check

#Open datafile to write in, glue arrays together, transpose, and save to data file	
datafile_id = open("heatsizetest.dat", 'w+')
data = np.array([ln, maxheats])
data = data.T
np.savetxt(datafile_id, data, fmt = ['%f', '%f'])
datafile_id.close()



